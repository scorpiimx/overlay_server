const express = require("express");
const app = express();
const { exec } = require("child_process");
const path  = require("path");
const fs = require("fs");
//const pupextra = require("./pufunextra.js");
const pupextra = {};
const moment = require('moment'); 
var bodyParser = require('body-parser');

var formidable = require('formidable');

//use bodyParser() to let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const activeWindow = require('active-win');

app.listen(3101, () => {
    if (!fs.existsSync("./req")) {
        fs.mkdirSync("req");
    }
    let reqs = fs.readdirSync(path.join(__dirname, 'req'));
    reqs.forEach(req => {
        //console.log('reading file:'+req);
        let reqJson = JSON.parse(fs.readFileSync(path.join(__dirname,'req/'+req)));
        //console.log(reqJson.inProgress);
        if(reqJson.inProgress){
            reqJson.inProgress = false;
            saveJSON(reqJson);
        }
        
    });
    console.log("El servidor está inicializado en el puerto 3101");
});

const exportDirectoryPath = path.join(__dirname, '/public');
console.log(exportDirectoryPath);
app.use('/public',express.static(exportDirectoryPath));

app.get('/', function (req, res) {
    res.sendFile(__dirname+"/dz.html");
});

app.post('/set_grade', function (req, res) {
    if(fs.existsSync("./req/grades.json")){
        var dataGrades = fs.readFileSync("./req/grades.json");
        dataGrades = JSON.parse(dataGrades);
    } else {
        var dataGrades = [];
    }
    
    const { grade, app } = req.body;
    dataGrades.push({
        grade:grade,
        app: app,
        time: moment().valueOf()
    });
    //dataGrades.slice(Math.max(dataGrades.length - 20, 0))
    fs.writeFileSync("./req/grades.json", JSON.stringify(dataGrades), null, 4);

    res.send('done');
});
app.post('/del_grade', function (req, res) {
    if(fs.existsSync("./req/grades.json")){
        var dataGrades = fs.readFileSync("./req/grades.json");
        dataGrades = JSON.parse(dataGrades);
    } else {
        var dataGrades = [];
    }
    
    const { id } = req.body;
    dataGrades.splice(id, 1);
    fs.writeFileSync("./req/grades.json", JSON.stringify(dataGrades), null, 4)

    res.send('done');
});
app.post('/up_grade', function (req, res) {
    if(fs.existsSync("./req/grades.json")){
        var dataGrades = fs.readFileSync("./req/grades.json");
        dataGrades = JSON.parse(dataGrades);
    } else {
        var dataGrades = [];
    }
    
    const { id, grade } = req.body;
    dataGrades[id].grade = grade
    fs.writeFileSync("./req/grades.json", JSON.stringify(dataGrades), null, 4)

    res.send('done');
});
app.post('/set_img', function (req, res) {
    if(fs.existsSync("./req/img.json")){
        var dataGrades = fs.readFileSync("./req/img.json");
        dataGrades = JSON.parse(dataGrades);
    } else {
        var dataGrades = [];
    }
    
    const { img } = req.body;
    dataGrades.push({
        img:img,
        time: moment().valueOf()
    });
    dataGrades.slice(Math.max(dataGrades.length - 20, 0))
    fs.writeFileSync("./req/img.json", JSON.stringify(dataGrades), null, 4);

    res.send('done');
});
app.post('/set_config', function (req, res) {
    if(fs.existsSync("./req/config.json")){
        var data = fs.readFileSync("./req/config.json");
        data = JSON.parse(data);
    } else {
        var data = {};
    }
    
    const { key, value } = req.body;
    data[key] = value;
    fs.writeFileSync("./req/config.json", JSON.stringify(data), null, 4);

    res.send('done');
});
app.post('/del_file', function (req, res) {
    const { file } = req.body;
    if(file == 'grades'){
        fs.unlinkSync("./req/img.json");
    }
    fs.unlinkSync("./req/"+file+".json");
    res.send('done');
});
app.post('/set_rule', function (req, res) {
    if(fs.existsSync("./req/rule.json")){
        var dataRules = fs.readFileSync("./req/rule.json");
        dataRules = JSON.parse(dataRules);
    } else {
        var dataRules = [];
    }
    
    const { type, id, value } = req.body;
    var newVal = {
        type: type,
        id: id,
        value: value
    };
    if(type=="app"){
        var found  =dataRules.find(elem =>
            (
                elem.type === type &&
                elem.id === id
            ));
            if(!found){
                dataRules.push(newVal);
            } else {
                found.value = value;
            }
        } else {
        var found  =dataRules.find(elem =>
            (
                elem.type === type &&
                elem.id === id &&
                elem.value === value
            ));
        if(!found){
            dataRules.push(newVal);
        } else  {
            dataRules = dataRules.filter(elem =>
                (
                    elem.value != value
                ));
        }
    }
    fs.writeFileSync("./req/rule.json", JSON.stringify(dataRules), null, 4);

    res.send('done');
});
app.get('/get_rule', function (req, res) {
    if(fs.existsSync("./req/rule.json")){
        var dataWindows = fs.readFileSync("./req/rule.json");
        dataWindows = JSON.parse(dataWindows);
    } else {
        var dataWindows = [];
    }
    res.send(dataWindows);
});
app.get('/get_config', function (req, res) {
    if(fs.existsSync("./req/config.json")){
        var data = fs.readFileSync("./req/config.json");
        data = JSON.parse(data);
    } else {
        var data = [];
    }
    res.send(data);
});
app.get('/get_grade', function (req, res) {
    if(fs.existsSync("./req/grades.json")){
        var dataWindows = fs.readFileSync("./req/grades.json");
        dataWindows = JSON.parse(dataWindows);
    } else {
        var dataWindows = [];
    }
    res.send(dataWindows);
});
app.get('/get_img', function (req, res) {
    if(fs.existsSync("./req/img.json")){
        var dataWindows = fs.readFileSync("./req/img.json");
        dataWindows = JSON.parse(dataWindows);
    } else {
        var dataWindows = [];
    }
    res.send(dataWindows);
});
app.get('/get_data', function (req, res) {
    if(fs.existsSync("./req/data.json")){
        var dataWindows = fs.readFileSync("./req/data.json");
        dataWindows = JSON.parse(dataWindows);
    } else {
        var dataWindows = {};
    }
    res.send(dataWindows);
});
app.get('/get_name', function (req, res) {
    (async () => {
        var data = await activeWindow();
        //console.log(data.title);
        //fs.writeFileSync("./req/" + vid.id, JSON.stringify(vid), null, 4);
        var isNew = false;
        if(fs.existsSync("./req/data.json")){
            var dataWindows = fs.readFileSync("./req/data.json");
            dataWindows = JSON.parse(dataWindows);
        } else {
            var dataWindows = {};
            isNew = true;
        }
        //console.log(dataWindows);
        if(!data){
            console.log('no hay data :S ')
        } else {
            if(!dataWindows[data.owner.name]){
                dataWindows[data.owner.name] = [];
                isNew = true;
            }
            if(dataWindows[data.owner.name].indexOf(data.title)<0){
                dataWindows[data.owner.name].push(data.title);
                isNew = true;
            }
            //console.log(isNew, dataWindows);
            if (isNew) {
                fs.writeFileSync("./req/data.json", JSON.stringify(dataWindows), null, 4)
            }
        }
        /*
        {
            title: 'Unicorns - Google Search',
            id: 5762,
            bounds: {
                x: 0,
                y: 0,
                height: 900,
                width: 1440
            },
            owner: {
                name: 'Google Chrome',
                processId: 310,
                bundleId: 'com.google.Chrome',
                path: '/Applications/Google Chrome.app'
            },
            url: 'https://sindresorhus.com/unicorn',
            memoryUsage: 11015432
        }
        */
        if(fs.existsSync("./req/rule.json")){
            var dataRules = fs.readFileSync("./req/rule.json");
            dataRules = JSON.parse(dataRules);
        } else {
            var dataRules = [];
        }
        if(data){

            var appId = data.owner.name.split(".")[0];
            var found = false;
            dataRules.forEach(rule => {
                    if(rule.type=="app"){
                     if(rule.type == "app" && rule.value == "true" && rule.id == appId) {
                         found = true;
                     }
                 } else {
                     if(rule.type == "title" && rule.value == data.title && rule.id == appId) {
                         found = true;
                     }
                 }
             });
            if(found){
                res.send(false);
            } else {
                res.send(data);
            }
        } else {
            res.send(false);
        }
    })();
});

app.get('/control', function (req, res) {
    res.sendFile(__dirname+"/control.html");
});


	/*
	{
		title: 'Unicorns - Google Search',
		id: 5762,
		bounds: {
			x: 0,
			y: 0,
			height: 900,
			width: 1440
		},
		owner: {
			name: 'Google Chrome',
			processId: 310,
			bundleId: 'com.google.Chrome',
			path: '/Applications/Google Chrome.app'
		},
		url: 'https://sindresorhus.com/unicorn',
		memoryUsage: 11015432
	}
	*/