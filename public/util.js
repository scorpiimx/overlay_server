var api = {
    get :function (endpoint, callback){
        $.ajax({
            url : endpoint, // Url of backend (can be python, php, etc..)
            async : true, // enable or disable async (optional, but suggested as false if you need to populate data afterwards)
            success: function(response, textStatus, jqXHR) {
                callback(response, textStatus, jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    },
    post : function (endpoint, data, callback ){
        $.ajax({
            url : endpoint, // Url of backend (can be python, php, etc..)
            method: "POST",
            data: data,
            async : true, // enable or disable async (optional, but suggested as false if you need to populate data afterwards)
            success: function(response, textStatus, jqXHR) {
                callback(response, textStatus, jqXHR);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
};